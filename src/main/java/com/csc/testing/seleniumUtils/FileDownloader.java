package com.csc.testing.seleniumUtils;


import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;

public class FileDownloader {
    private static final Logger logger = LoggerFactory.getLogger(FileDownloader.class);
    private String downloadPath = System.getProperty("java.io.tmpdir");
    private String downloadFile = "downloaded_pdf.pdf";

    public FileDownloader() {
    }

    public FileDownloader(String saveAs) {
        downloadFile = saveAs;
    }

    public String getFullDownloadPath() {
        return this.downloadPath + this.downloadFile;
    }

    /**
     * Get the current location that files will be downloaded to.
     *
     * @return The filepath that the file will be downloaded to.
     */
    public String getDownloadPath() {
        return this.downloadPath;
    }

    /**
     * Set the path that files will be downloaded to.
     *
     * @param filePath The filepath that the file will be downloaded to.
     */
    public void setDownloadPath(String filePath) {
        this.downloadPath = filePath;
    }

    /**
     * Mimic the WebDriver host configuration
     */
    private HostConfiguration mimicHostConfiguration(String hostURL, int hostPort) {
        HostConfiguration hostConfig = new HostConfiguration();
        hostConfig.setHost(hostURL, hostPort);
        return hostConfig;

    }

    public String fileDownloader(WebElement element) throws Exception {
        return downloader(element, "href");
    }

    public String imageDownloader(WebElement element) throws Exception {
        return downloader(element, "src");
    }

    private String downloader(WebElement element, String attribute) throws Exception {
        //Assuming that getAttribute does some magic to return a fully qualified URL
        String downloadLocation = element.getAttribute(attribute);
        if (downloadLocation.trim().equals("")) {
            throw new IllegalArgumentException("The element you have specified does not link to anything!");
        }

        URL downloadURL = new URL(downloadLocation);
        HttpClient client = new HttpClient();
        client.getParams().setCookiePolicy(CookiePolicy.RFC_2965);
        client.setHostConfiguration(mimicHostConfiguration(downloadURL.getHost(), downloadURL.getPort()));

        HttpMethod getRequest = new GetMethod(downloadURL.getPath());
        FileHandler downloadedFile = new FileHandler(downloadPath + downloadFile, true);
        try {
            int status = client.executeMethod(getRequest);
            logger.debug("HTTP Status {} when getting '{}'", status, downloadURL.toExternalForm());
            BufferedInputStream in = new BufferedInputStream(getRequest.getResponseBodyAsStream());
            int offset = 0;
            int len = 4096;
            int bytes;
            byte[] block = new byte[len];
            while ((bytes = in.read(block, offset, len)) > -1) {
                downloadedFile.getWritableFileOutputStream().write(block, 0, bytes);
            }

            downloadedFile.close();
            in.close();
            logger.debug("File downloaded to '{}'", downloadedFile.getAbsoluteFile());

        } catch (Exception ex) {
            logger.error("Download failed: {}", ex);
            throw new IOException("Download failed!");
        } finally {
            getRequest.releaseConnection();
        }

        return downloadedFile.getAbsoluteFile();
    }
}
