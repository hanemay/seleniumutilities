package com.csc.testing.seleniumUtils;

import net.jsourcerer.webdriver.jserrorcollector.JavaScriptError;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class JavascriptExecutions {
    private static final Logger logger = LoggerFactory.getLogger(StringManipulation.class);
    @Autowired
    public String url;
    @Autowired
    private WebDriver driver;

    private JavascriptExecutor javascriptExecutor;

    @Autowired
    private SeleniumUtils sUtils;

    public JavascriptExecutions(JavascriptExecutor javascriptExecutor) {
        this.javascriptExecutor = javascriptExecutor;
    }

    /**
     * use this method to check for js errors on the current url
     */
    public void checkForJSErrors() {
        List<JavaScriptError> Errors = JavaScriptError.readErrors(driver);
        if (!Errors.isEmpty()) {
            StringBuilder error = new StringBuilder();
            for (int i = 0; i < Errors.size(); i++) {
                error.append("Error Message : ").append(Errors.get(i).getErrorMessage());
                error.append("\nError Line No : ").append(Errors.get(i).getLineNumber());
                error.append("\n error on ").append(driver.getCurrentUrl()).append(" on script ")
                        .append(Errors.get(i).getSourceName()).append("\n");
            }
            if (!error.toString().contains("mutating the [[Prototype]] of an object will cause your code to run very slowly;")) {
                throw new AssertionError("\nExpected: is <0>\n" + "but was <" + Errors.size() + ">\n" + error);
            }
        }
    }

    /**
     * send text to element through javascript
     *
     * @param msg  message
     * @param elem element to send to
     */
    public void sendTextWithJavaScript(String msg, WebElement elem) {
        executeJavascript("arguments[0].setAttribute('value', '" + msg + "')", elem);
    }


    /**
     * @param elem element to execute against
     */
    public String executeJavascript(String javascript, WebElement elem) {
        return javascriptExecutor.executeScript(javascript, elem).toString();
    }

    /**
     * if normal click method for selenium failes use this method, tager elementer der skal klikkes på
     * som parameter
     */
    public void clickWithJavascript(WebElement elem) {
        javascriptExecutor.executeScript("arguments[0].click();", elem);
    }

    /**
     * @return result from javascript execution
     */
    public String executeJavascript(String javascript) {
        try {
            return javascriptExecutor.executeScript(javascript).toString();
        } catch (NullPointerException npe) {
//intet behov for at logge her !
            return null;
        }
    }

    /**
     * @return navigate to site and validate we are there
     */
    public void navigateTo(String navigationUrl) {
        boolean areWeThere = false;
        try {
            for (int i = 0; i < 10; i++) {
                javascriptExecutor.executeScript("window.location = \'" + navigationUrl + "\'");
                sUtils.waitForLoadedPage();
                logger.debug(driver.getCurrentUrl());

                if (driver.getCurrentUrl().contains(navigationUrl)) {
                    break;
                }
            }
        } catch (NullPointerException npe) {
//intet behov for at logge her !
        }
    }

    /**
     * zooms out or in
     */
    public void zoom(int percent) {
        if (percent > 100 && percent < 0) {
            throw new RuntimeException("dont zoom below above 100 or below 0");
        } else {
            executeJavascript("document.body.style.zoom = '" + percent + "%'");
        }
    }

}
