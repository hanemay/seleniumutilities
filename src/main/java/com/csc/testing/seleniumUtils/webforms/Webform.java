package com.csc.testing.seleniumUtils.webforms;

import java.util.List;

/**
 * Created by nbade2 on 19-06-2017.
 */
public class Webform {
    private List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
