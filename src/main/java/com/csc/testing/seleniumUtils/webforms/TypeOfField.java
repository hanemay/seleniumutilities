package com.csc.testing.seleniumUtils.webforms;

/**
 * Created by nbade2 on 19-06-2017.
 */
public enum TypeOfField {
    RADIO, CHECK, INPUT, TEXTAREA
}
