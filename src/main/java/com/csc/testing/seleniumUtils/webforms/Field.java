package com.csc.testing.seleniumUtils.webforms;

import org.openqa.selenium.WebElement;

public class Field {
    private String selector;
    private WebElement field;
    private TypeOfField typeOfField;

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public WebElement getField() {
        return field;
    }

    public void setField(WebElement field) {
        this.field = field;
    }

    public TypeOfField getTypeOfField() {
        return typeOfField;
    }

    public void setTypeOfField(TypeOfField typeOfField) {
        this.typeOfField = typeOfField;
    }
}
