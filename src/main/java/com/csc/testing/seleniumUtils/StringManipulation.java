package com.csc.testing.seleniumUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StringManipulation {
    private static final Logger logger = LoggerFactory.getLogger(StringManipulation.class);

    /**
     * sets first letter of a given string to upper case
     */
    public String setFirstLetterUpperCase(String selector) {
        return selector.substring(0, 1).toUpperCase() + selector.substring(1, selector.length());
    }

    /**
     * sets first letter of a given string to upper case
     */
    public String setFirstLetterUpperCaseAndRestLover(String selector) {
        return selector.substring(0, 1).toUpperCase() + selector.substring(1, selector.length()).toLowerCase();
    }

    /**
     * sets first letter of a given string to lower case
     */
    public String setFirstLetterLowerCase(String selector) {
        return selector.substring(0, 1).toLowerCase() + selector.substring(1, selector.length());
    }

    public String removeQoutesFromString(String selector) {
        try {
            return selector.replaceAll("\"", "");
        } catch (NullPointerException npe) {
            logger.debug(npe.getMessage(), npe);
            return selector;
        }
    }

    /*
    @param jsonObject det json object der skal manipuleres med
    @param hele den json der manipuleres i
    @param nyVaerdi den nye værdi der skal tilføjes
    @param add hvis add er true bliver ny værdi ikke brugt, istedet vil værdi bare plusses med 1
     */
    public String replaceInJson(String jsonObject, String json, String nyVaerdi, boolean add) {
        int firstIndex = json.indexOf(jsonObject);
        Double number = null;
        boolean skip = false;
        try {
            number = Double.parseDouble(json.substring(firstIndex + jsonObject.length(), json.indexOf(",", firstIndex)));
        } catch (IndexOutOfBoundsException ioob) {
            throw new IndexOutOfBoundsException("fejl ved " + jsonObject);
        } catch (NumberFormatException nfe) {
            skip = true;
        }
        if (!skip) {
            if (add | nyVaerdi == null) {
                number = number + 1;
            } else {
                try {
                    number = Double.parseDouble(nyVaerdi);
                } catch (NumberFormatException nfe) {
                    nyVaerdi = nyVaerdi.replaceAll("\"", "");
                    number = Double.parseDouble(nyVaerdi);
                }
            }
            if ((number == Math.floor(number)) && !Double.isInfinite(number)) {
                return json.substring(0, firstIndex + jsonObject.length()) + number + json.substring(json.indexOf(",", firstIndex));
            } else {
                return json.substring(0, firstIndex + jsonObject.length()) + number.intValue() + json.substring(json.indexOf(",", firstIndex));
            }
        } else {
            return json.substring(0, firstIndex + jsonObject.length()) + nyVaerdi + json.substring(json.indexOf(",", firstIndex));
        }
    }

    public String getUrlForExternal() {
        String url;
        if ((System.getProperty("URL").toLowerCase().contains("test") || System.getProperty("URL").toLowerCase().contains("data")) && !System.getProperty("URL").toLowerCase().contains("int") && !System.getProperty("URL").toLowerCase().contains("ext")) {
            url = "http://kom-" + System.getProperty("URL").toLowerCase() + ".csc.dk:8080";
        } else {
            url = "https://kom-ee-" + System.getProperty("URL").toLowerCase()
                    + "-api.csc.dk";
        }
        return url;
    }

}
