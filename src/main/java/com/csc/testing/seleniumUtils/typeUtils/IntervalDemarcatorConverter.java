package com.csc.testing.seleniumUtils.typeUtils;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class IntervalDemarcatorConverter implements Converter<String, LocalDate> {

    private static final Pattern omAntalDage = Pattern.compile("om (-?\\d+)+ dage");

    @Override
    public LocalDate convert(String source) {
        Assert.notNull(source, "Source was null");
        String localSource = source.trim().toLowerCase();
        if ("idag".equals(localSource)) {
            return LocalDate.now();
        } else if ("åben".equals(localSource)) {
            return null;
        } else {
            Matcher omAntalDateMatcher = omAntalDage.matcher(localSource);
            if (omAntalDateMatcher.matches()) {
                // conversion guarded by the pattern
                int offestInDays = Integer.parseInt(omAntalDateMatcher.group(1));
                return LocalDate.now().plusDays(offestInDays);
            }
        }
        throw new IllegalArgumentException("Pattern not supported as a \"start dato\": " + localSource);
    }
}
