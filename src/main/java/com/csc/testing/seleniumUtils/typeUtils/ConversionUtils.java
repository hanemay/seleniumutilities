package com.csc.testing.seleniumUtils.typeUtils;

import com.csc.testing.seleniumUtils.HashTagKoder;
import com.google.common.collect.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

@Component
public class ConversionUtils {
    private static final Logger logger = LoggerFactory.getLogger(ConversionUtils.class);
    @Autowired
    private IntervalDemarcatorConverter intervalDemarcatorConverter;
    @Autowired
    private LocalDateIntervalPrinter localDateIntervalPrinter;

    public static String getNumberWithDotIfBeforeCurrentDate(String value, String date,
                                                             String antalDage) throws IOException {
        String oldValue = "";
        if (value.contains("#")) {
            value = HashTagKoder.returnPartialHashTag(value);
        }
        if (date.contains("#")) {
            date = HashTagKoder.returnPartialHashTag(date);
            date = returnDateFromStamoplysninger(date, antalDage);
        }
        try {
            oldValue = value.substring(value.indexOf(" "), value.length());
            value = value.substring(0, value.indexOf(" "));
        } catch (StringIndexOutOfBoundsException sioob) {

        }
        try {
            LocalDate dateConverted = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.uuuu"));
            if (isBefore(dateConverted)) {
                return returnWithDotSeperator(value);
            } else {
                return value + oldValue;
            }
        } catch (DateTimeParseException dtpe) {
            return value;
        }
    }

    public static String returnDateFromStamoplysninger(String date, String antalDage)
            throws IOException {
        LocalDate dateConverted = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.uuuu"));
        dateConverted = dateConverted.minusDays(Long.parseLong(antalDage));
        return dateConverted.getDayOfMonth() + "." + dateConverted.getMonthValue() + "." + dateConverted
                .getYear();
    }

    private static String returnWithDotSeperator(String numberToConvert) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.ENGLISH);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number number = null;
        try {
            number = format.parse(numberToConvert);
        } catch (ParseException e) {
        }
        double value = number.doubleValue();
        return String.valueOf(formatter.format(value));
    }

    public static boolean isBefore(LocalDate dateBeforeConverted) {
        return dateBeforeConverted.isBefore(LocalDate.now());
    }

    public String rangeSpecToUiOutput(String startSpec, String endSpec) {
        LocalDate start = intervalDemarcatorConverter.convert(startSpec);
        LocalDate end = intervalDemarcatorConverter.convert(endSpec);

        Range range = null;
        if (end != null) {
            if (start != null) {
                range = Range.closed(start, end);
            }
        } else if (start != null) {
            range = Range.atLeast(start);
        }
        if (range == null) {
            throw new RuntimeException("Range spec not supported: " + start + ", " + end);
        }
        return localDateIntervalPrinter.print(range, PresentationFlavor.UiOutput.getLocale());
    }

    public String dateSpecToUserInput(String dateSpec) {
        LocalDate date = intervalDemarcatorConverter.convert(dateSpec);
        return date.format(PresentationFlavor.UserInput.getDateTimeFormatter());
    }
}
