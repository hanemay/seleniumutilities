package com.csc.testing.seleniumUtils.typeUtils;

import com.google.common.collect.Lists;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.List;

public class KeyStrokeManipulation {
    public void pressKey(int vkKey) {
        Robot robot = getRobot();
        assert robot != null;
        robot.keyPress(vkKey);
        sleep(100);
        robot.keyRelease(vkKey);
    }

    private void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pressKeys(List<Integer> keys) {
        Robot robot = getRobot();
        assert robot != null;
        Robot finalRobot = robot;
        keys.stream().forEach(key -> {
            finalRobot.keyPress(key);
            sleep(100);
        });
        sleep(100);
        keys = Lists.reverse(keys);
        keys.stream().forEach(key -> {
            finalRobot.keyRelease(key);
            sleep(100);
        });
    }

    private Robot getRobot() {
        try {
            return new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void click(int x, int y) {
        Robot robot = getRobot();
        robot.mouseMove(x, y);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void clickCenter() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = screenSize.width / 2;
        int centerY = screenSize.height / 2;
        click(centerX, centerY);
    }
}
