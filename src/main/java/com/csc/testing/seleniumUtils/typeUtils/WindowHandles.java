package com.csc.testing.seleniumUtils.typeUtils;

import java.awt.*;

public class WindowHandles {

    public Window getSelectedWindow(Window[] windows) {
        Window result = null;
        for (Window window : windows) {
            if (window.isActive()) {
                result = window;
            } else {
                Window[] ownedWindows = window.getOwnedWindows();
                if (ownedWindows != null) {
                    result = getSelectedWindow(ownedWindows);
                }
            }
        }
        return result;
    }
}
