package com.csc.testing.seleniumUtils.typeUtils;

import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UnknownFormatConversionException;

/**
 * The dates are currently presented by the UI in two different flavors depending
 * on whether they are being reported by the UI or entered by the user
 *
 * @since 12/10-2016
 */
public enum PresentationFlavor {
    UiOutput {
        @Override
        public DateTimeFormatter getDateTimeFormatter() {
            return DateTimeFormatter.ofPattern("dd.MM.yyy");
        }
    },
    UserInput {
        @Override
        public DateTimeFormatter getDateTimeFormatter() {
            return DateTimeFormatter.ofPattern("dd/MM/yyy");
        }
    },;

    public static DateTimeFormatter getDateTimeFormatterForLocale(Locale locale) {
        return byLocale(locale).getDateTimeFormatter();
    }

    public static PresentationFlavor byLocale(Locale locale) {
        if (UiOutput.getLocale().equals(locale)) {
            return UiOutput;
        } else if (UserInput.getLocale().equals(locale)) {
            return UserInput;
        }
        throw new UnknownFormatConversionException("Unrecognized locale: " + locale);
    }

    public abstract DateTimeFormatter getDateTimeFormatter();

    public Locale getLocale() {
        return new Locale("da", "DK", name());
    }
}
