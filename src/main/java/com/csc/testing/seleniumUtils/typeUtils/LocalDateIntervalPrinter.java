package com.csc.testing.seleniumUtils.typeUtils;

import com.google.common.collect.Range;
import org.springframework.format.Printer;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @since 12/10-2016
 */
@Component
public class LocalDateIntervalPrinter implements Printer<Range<LocalDate>> {

    @Override
    public String print(Range<LocalDate> range, Locale locale) {
        StringBuilder buf = new StringBuilder();
        DateTimeFormatter formatter = PresentationFlavor.getDateTimeFormatterForLocale(locale);
        buf.append(range.lowerEndpoint().format(formatter));
        if (range.hasUpperBound()) {
            buf.append("-");
            buf.append(range.upperEndpoint().format(formatter));
        }
        return buf.toString();
    }
}
