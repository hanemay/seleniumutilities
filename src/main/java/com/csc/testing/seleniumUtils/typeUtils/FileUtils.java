package com.csc.testing.seleniumUtils.typeUtils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public File findFileNew(String fileName) {

        String fullPath;
        try {
            String rootPath = new File(".").getCanonicalPath();

            List<String> fileNames = new ArrayList<>();
            fileNames.add("/bidragsgrundlag/" + fileName);
            fileNames.add("/kommune-logos/" + fileName);
            fileNames.add("/bidragTestdata/" + fileName);
            fileNames.add("/testresources/" + fileName);
            for (String fileToFind : fileNames) {
                fullPath = rootPath + fileToFind;
                File currentDirFile = new File(fullPath);
                if (currentDirFile.exists()) {
                    return currentDirFile;
                }
            }
        } catch (IOException ignored) {
            logger.error(ignored.getMessage(), ignored);
        }

        return null;
    }

    /**
     * Opretter en temporær fil hvor indholdet bliver gemt i
     *
     * @param gemSom      Navnet på filen (extenstion .pdf)
     * @param fileContent Indholdet af filen/PDF
     * @return Stien til hvor PDF filen er gemt
     */
    public String SaveTempPDFFile(String gemSom, byte[] fileContent) {
        File outFile;
        String result = "";
        try {
            outFile = File.createTempFile(gemSom, ".pdf");
            outFile.deleteOnExit();

            try (FileOutputStream fileOutputStream = new FileOutputStream(outFile)) {
                fileOutputStream.write(fileContent);
                fileOutputStream.flush();
                fileOutputStream.close();
            }

            result = outFile.getAbsolutePath();
        } catch (IOException e) {
            logger.error("Kunne ikke gemme temp-PDF " + gemSom, e);
        }

        return result;
    }

    /**
     * Loader en temporær PDF fil
     *
     * @param filNavn Navnet på filen (extenstion .pdf)
     * @return Stien til hvor PDF filen er gemt
     */
    public PDDocument loadTempPDFFile(String filNavn) throws IOException {
        PDDocument document = null;
        try {
            document = PDDocument.load(new File(filNavn));
        } catch (IOException e) {
            logger.error("Kunne ikke loade tempfil " + filNavn, e);
        } finally {
            document.close();
        }

        return document;
    }
}
