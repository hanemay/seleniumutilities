package com.csc.testing.seleniumUtils.lists;

import java.util.List;

/**
 * Created by nbade2 on 19-06-2017.
 */
public class TableElements {
    private String fullElementText;
    private List<NedstedTableValues> nedstedTableValues;

    public TableElements(String fullElementText) {
        this.fullElementText = fullElementText;
    }

    public String getFullElementText() {
        return fullElementText;
    }

    public void setFullElementText(String fullElementText) {
        this.fullElementText = fullElementText;
    }

    public List<NedstedTableValues> getNedstedTableValues() {
        return nedstedTableValues;
    }

    public void setNedstedTableValues(List<NedstedTableValues> nedstedTableValues) {
        this.nedstedTableValues = nedstedTableValues;
    }
}
