package com.csc.testing.seleniumUtils.lists;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class NedstedTableValues {
    private List<String> valuesInElement;

    public List<String> getValuesInElement() {
        return valuesInElement;
    }

    public void setValuesInElement(List<WebElement> webElements) {
        valuesInElement = new ArrayList<>();
        for (WebElement element : webElements) {
            valuesInElement.add(element.getText());
        }
    }
}
