package com.csc.testing.seleniumUtils;

/**
 * Created by nbade2 on 10-07-2017.
 */
public class findElementStrings {

    public static final String API = "/api/";
    public static final String NORMALIZETEXT = "//*[normalize-space(text())='";
    public static final String SCROLLTO = "window.scrollTo";
    public static final String VALUE = "value";
    public static final String TIMEOUT = "Timeout: ";
    public static final String COULDNOTFINDELEMENT = "\nCouldnt find element : ";
    public static final String ATTRIBUTECHECKED = "checked";
    public static final String IDANDNORMALIZESPACEDOT = "//*[@id=(//*[normalize-space(.)='";
    public static final String IDANDNORMALIZESPACETEXT = "//*[@id=(//*[normalize-space(text())='";
    public static final String NAMEWITHFOR = "//*[@name='";
    public static final String ENDFOR = "'])/@for]";
    public static final String FALSE = "false";
    public static final String EMPTY = "empty value";
    public static final String DISABLED = "disabled";
    public static final String FIELDSETDISABLED = "']/fieldset[@disabled]";
    public static final String NORMALIZEDOT = "//*[normalize-space(.)='";


}
