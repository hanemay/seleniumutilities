package com.csc.testing.seleniumUtils.elementVerifications;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class ElementVadility {

    public boolean isStale(WebElement inputField) {
        try {
            if (inputField.isEnabled() || inputField.isDisplayed()) {
                return false;
            }
            return false;
        } catch (StaleElementReferenceException stale) {
            return true;
        }
    }

    public boolean isValid(WebElement inputField) {
        try {
            inputField.isEnabled();
            return false;
        } catch (InvalidElementStateException | StaleElementReferenceException | NullPointerException stale) {
            return true;
        }
    }
}
