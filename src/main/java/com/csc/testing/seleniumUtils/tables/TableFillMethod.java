package com.csc.testing.seleniumUtils.tables;

/**
 * Created by nbade2 on 19-06-2017.
 */
public enum TableFillMethod {
    ROWANDNAMEINVALUE, TABLEWITHVALUEINROW
}
