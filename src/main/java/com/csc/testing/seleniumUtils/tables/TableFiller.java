package com.csc.testing.seleniumUtils.tables;


import com.csc.testing.seleniumUtils.SeleniumUtils;
import com.csc.testing.seleniumUtils.StringManipulation;
import com.csc.testing.seleniumUtils.elementVerifications.ElementVadility;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static com.csc.testing.seleniumUtils.findElementStrings.NORMALIZETEXT;


public class TableFiller {
    private ElementVadility elementVadility = new ElementVadility();
    private SeleniumTable currentTable;
    private SeleniumUtils sUtils;

    public TableFiller(SeleniumUtils sUtils) {
        this.sUtils = sUtils;
    }

    private void setCurrentTable() {
        switch (currentTable.getTableFillMethod()) {
            case TABLEWITHVALUEINROW:
                currentTable = getTableWithValuesAndNameInRow(currentTable.getTableName());
                break;
            case ROWANDNAMEINVALUE:
                currentTable = getTableWithValuesInRow(currentTable.getTableName());
                break;
        }
    }

    /**
     * if table is stale generate a new one
     */
    public SeleniumTable getCurrentTable() {
        try {
            if (elementVadility.isStale(currentTable.getSeleniumTableRows().get(0).getFullTable())) {
                setCurrentTable();
            }
        } catch (IndexOutOfBoundsException iooe) {
            setCurrentTable();
        }
        return currentTable;
    }

    public void setCurrentTable(SeleniumTable currentTable) {
        this.currentTable = currentTable;
    }

    /**
     * if table is stale generate a new one
     */
    public SeleniumTable getCurrentTableWithSpecificElement(WebElement element, boolean trySendKeys) {
        try {
            if (elementVadility.isStale(element)) {
                setCurrentTable();
            }
            if (trySendKeys) {
                element.sendKeys("1");
                element.clear();
            }
        } catch (IndexOutOfBoundsException | StaleElementReferenceException see) {
            setCurrentTable();
        }
        return currentTable;
    }

    /**
     * finds a table and returns it as a list with a table name and text what column to find ie ("Ejer information","Navn")
     *
     * @param tableName the text above the table
     * @return SeleniumTable
     */
    public SeleniumTable getTableWithValuesAndNameInRow(String tableName) {
        SeleniumTable seleniumTable = new SeleniumTable(tableName);
        String type = checkForTypes(tableName);
        List<WebElement> element;
        try {
            element = sUtils.findElementsByXpathAndWait(
                    NORMALIZETEXT + tableName + "']/ancestor::div[1]/descendant::table");

        } catch (NoSuchElementException nsee) {
            try {
                element = sUtils.findElementsByXpathAndWait(
                        NORMALIZETEXT + tableName + "']/ancestor::div[1]/descendant::table/*/*");
            } catch (NoSuchElementException nsee0) {
                element = sUtils
                        .findElementsByXpathAndWait(NORMALIZETEXT + tableName + "']/ancestor::" +
                                type + "/descendant::table/*/*");
            }
        }
        seleniumTable.setSeleniumRowValues(element);
        seleniumTable.setTableFilleMethod(TableFillMethod.TABLEWITHVALUEINROW);
        List<SeleniumTableRow> seleniumTableRows = seleniumTable.getSeleniumTableRows();
        for (SeleniumTableRow seleniumTableRow : seleniumTableRows) {
            WebElement findNestedRows = seleniumTableRow.getFullTable();
            List<WebElement> webElements = new ArrayList<>();
            try {
                webElements = sUtils.findElementsByXpathAndWait(findNestedRows,  "./descendant::tr");
            } catch (NoSuchElementException nsee) {
                try {
                    webElements = sUtils.findElementsByXpathAndWait(findNestedRows,  "./descendant::*");
                } catch (NoSuchElementException nsee1) {
                }
            }
            if (webElements.size() != 0) {
                seleniumTableRow.setSeleniumRows(webElements, false);
            }
        }

        setCurrentTable(seleniumTable);
        return seleniumTable;
    }

    /**
     * finds a table and returns it as a list with a table name and text what column to find ie ("Ejer information","Navn")
     *
     * @param tableName the text above the table
     * @return SeleniumTable
     */
    public SeleniumTable getTableWithValuesInRow(String tableName) {
        SeleniumTable seleniumTable = new SeleniumTable(tableName);
        String type = checkForTypes(tableName);
        List<WebElement> element;
        try {
            element = sUtils.findElementsByXpathAndWait(
                    NORMALIZETEXT + tableName + "']/ancestor::div[1]/descendant::table/*/*");
            if (element.size() == 0) {
                throw new NoSuchElementException("size var 0 så vi går i nested");
            }
        } catch (NoSuchElementException nsee) {
            element = sUtils.findElementsByXpathAndWait(NORMALIZETEXT + tableName + "']/ancestor::" +
                    type + "/descendant::table/*/*");
        }
        seleniumTable.setSeleniumRowValuesWithColumnHeaders(element);
        seleniumTable.setTableFilleMethod(TableFillMethod.ROWANDNAMEINVALUE);
        List<SeleniumTableRow> seleniumTableRows = seleniumTable.getSeleniumTableRows();
        for (SeleniumTableRow seleniumTableRow : seleniumTableRows) {
            WebElement findNestedRows = seleniumTableRow.getFullTable();
            List<WebElement> webElements;
            try {
                webElements = sUtils.findElementsByXpathAndWait(findNestedRows,  "./descendant::tr");

            } catch (NoSuchElementException nsee) {
                try {
                    webElements = sUtils.findElementsByXpathAndWait(findNestedRows,  "./descendant::td");
                } catch (NoSuchElementException fixMe) {
                    webElements = element;
                }
            }
            seleniumTableRow.setSeleniumRows(webElements, true);
        }
        setCurrentTable(seleniumTable);
        return seleniumTable;
    }


    private String checkForTypes(String type) {
        if (type.toLowerCase().contains("bidrag") && !type.toLowerCase().contains("amortisable")) {
            if (type.contains(sUtils.checkForHashTags("#nuværendeår"))) {
                return "//*[@data-skatteaar='vm.currentYear']/descendant::table/*/* ";
            } else {
                return "//*[@data-skatteaar='vm.nextYear']/descendant::table/*/* ";
            }
        }
        switch (type.toLowerCase()) {
            case "amortisable bidrag":
                return "ee-amortisable-bidrag";
            case "grundlag for beregning":
                return "beregningsgrundlag-sag";
            case "beslutninger":
                return "beslutningdaekningsafgift";
            case "skatter og afgifter":
                return "skatter-afgifter-sag";
            case "administratorinformation":
                return "administratorsag";
            case "fordeling":
                return "fordeling";
            case "specifikationslinjer på ejendomsskattebilletten":
                return "specifikation-sag";
            case "ratefordeling":
                return "ratefordeling-sag";
            case "satsermarkeringer":
                return "satsermarkeringer";
            default:
                StringManipulation stringManipulation = new StringManipulation();
                return stringManipulation.setFirstLetterLowerCase(type).replaceAll(" ", "-");
        }
    }
}
