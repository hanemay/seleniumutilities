package com.csc.testing.seleniumUtils.tables;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by nbade2 on 19-06-2017.
 */
public class SeleniumColumn {
    private String rowName;
    private String rowValue;
    private boolean useName = false;
    private boolean hasColumnHeaders = false;
    private List<String> rowHeaders;
    private WebElement inputField;
    private String firstColumnText;

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }

    public boolean isUseName() {
        return useName;
    }

    public void setUseName(boolean useName) {
        this.useName = useName;
    }

    public boolean isHasColumnHeaders() {
        return hasColumnHeaders;
    }

    public void setHasColumnHeaders(boolean hasColumnHeaders) {
        this.hasColumnHeaders = hasColumnHeaders;
    }

    public List<String> getRowHeaders() {
        return rowHeaders;
    }

    public void setRowHeaders(List<String> rowHeaders) {
        this.rowHeaders = rowHeaders;
    }

    public String getRowValue() {
        updateResults();
        if (rowValue == null) {
            return null;
        } else {
            return rowValue;
        }
    }

    private void setRowValue(String rowValue) {
        this.rowValue = rowValue;
    }

    public WebElement getInputField() {
        return inputField;
    }

    public void setInputField(WebElement inputField) {
        this.inputField = inputField;
        setRowValue(getRowValue(inputField));
    }

    private void sendKeys(boolean clear, String value) {
        if (clear) {
            inputField.clear();
        }
        inputField.sendKeys(value);
        inputField.sendKeys(Keys.TAB);
    }

    public void clearAndSendKeys(String inputString) {
        sendKeys(true, inputString);
        updateResults();
    }

    public void sendKeys(String inputString) {
        sendKeys(false, inputString);
        updateResults();
    }

    private void updateResults() {
        setRowValue(getRowValue(inputField));
    }

    private String getRowValue(WebElement element) {
        String rowValue = element.getText();
        if (rowValue.equals("")) {
            rowValue = element.getAttribute("value");
        }
        return rowValue;
    }

    public void setFirstColumnInRow(String firstColumnText) {
        this.firstColumnText = firstColumnText;
    }

    public String getFirstColumnText() {
        return firstColumnText;
    }
}
