package com.csc.testing.seleniumUtils.tables;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static com.csc.testing.seleniumUtils.typeUtils.EditDistance.editDistance;

/**
 * Created by nbade2 on 19-06-2017.
 */
public class SeleniumTable {
    private boolean partial;
    private String tableName;
    private ArrayList<SeleniumTableRow> seleniumTableRows;
    private TableFillMethod tableFillMethod;
    private List<SeleniumColumn> multipleColumnSearchResults;

    public SeleniumTable(String tableName) {
        this.tableName = tableName;
    }

    private static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2;
            shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) {
            return 1.0; /* both strings are zero length */
        }
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
    }

    public ArrayList<SeleniumTableRow> getSeleniumTableRows() {
        return seleniumTableRows;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setSeleniumRowValues(List<WebElement> webElementList) {
        seleniumTableRows = new ArrayList<>();
        for (WebElement element : webElementList) {
            SeleniumTableRow seleniumTableRow = new SeleniumTableRow();
            seleniumTableRow.setFullTable(element);
            seleniumTableRows.add(seleniumTableRow);
        }
    }

    public boolean isPartial() {
        return partial;
    }

    public void setPartial(boolean partial) {
        this.partial = partial;
    }

    public SeleniumColumn findRowName(String name) {
        for (SeleniumTableRow seleniumTableRow : seleniumTableRows) {
            List<SeleniumColumn> seleniumColumns = seleniumTableRow.getSeleniumColumns();
            if (seleniumColumns != null) {
                for (SeleniumColumn seleniumColumn : seleniumColumns) {
                    if (seleniumColumn.isUseName()) {
                        if (seleniumColumn.getRowName().contains(name)) {
                            return seleniumColumn;
                        }
                    } else {
                        if (seleniumColumn.getRowValue().contains(name)) {
                            return seleniumColumn;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void setSeleniumRowValuesWithColumnHeaders(List<WebElement> webElementList) {
        seleniumTableRows = new ArrayList<>();
        List<WebElement> columnHeaders = webElementList.get(0).findElements(By.xpath("./descendant::th"));
        webElementList.remove(0);
        for (WebElement element : webElementList) {
            SeleniumTableRow seleniumTableRow = new SeleniumTableRow();
            seleniumTableRow.setFullTable(element);
            seleniumTableRow.setColumnHeaders(element, columnHeaders);
            seleniumTableRows.add(seleniumTableRow);
        }
    }

    public SeleniumColumn findRowNameWithBegginningColumn(String name, String online) {
        boolean found = false;
        SeleniumColumn seleniumColumnNew = null;
        for (SeleniumTableRow seleniumTableRow : seleniumTableRows) {

            List<SeleniumColumn> seleniumColumns = seleniumTableRow.getSeleniumColumns();
            for (SeleniumColumn seleniumColumn : seleniumColumns) {
                try {
                    if (seleniumColumn.isUseName()) {
                        if (seleniumColumn.getRowName().contains(name) && seleniumColumn.getFirstColumnText().contains(online)) {
                            if (found) {
                                seleniumColumnNew = fetchCorrect(seleniumColumnNew, seleniumColumn, true, online);
                            } else {
                                seleniumColumnNew = seleniumColumn;
                                found = true;
                            }
                        }
                    } else {
                        if (seleniumColumn.getRowValue().contains(name) && seleniumColumn.getFirstColumnText().contains(online)) {
                            if (found) {
                                seleniumColumnNew = fetchCorrect(seleniumColumnNew, seleniumColumn, false, online);
                            } else {
                                seleniumColumnNew = seleniumColumn;
                                found = true;
                            }
                        }
                    }
                } catch (NullPointerException npe) {
                    //hvis der er null i tabel led videre
                }
            }

        }
        if (found) {
            return seleniumColumnNew;
        }
        return new SeleniumColumn();
    }

    private SeleniumColumn fetchCorrect(SeleniumColumn seleniumColumnNew, SeleniumColumn seleniumColumn, boolean useName, String name) {
        double newColumn = similarity(seleniumColumnNew.getRowValue(), name);
        double oldColumn = similarity(seleniumColumn.getRowValue(), name);
        if (newColumn < oldColumn) {
            return seleniumColumn;
        } else {
            return seleniumColumnNew;
        }
    }

    public void setTableFilleMethod(TableFillMethod tableFillMethod) {
        this.tableFillMethod = tableFillMethod;
    }

    public TableFillMethod getTableFillMethod() {
        return tableFillMethod;
    }

    public List<SeleniumColumn> findRowsNameWithBegginningColumn(String name, String online) {
        boolean found = false;
        this.multipleColumnSearchResults = new ArrayList<>();
        SeleniumColumn seleniumColumnNew = null;
        try {
            for (SeleniumTableRow seleniumTableRow : seleniumTableRows) {
                List<SeleniumColumn> seleniumColumns = seleniumTableRow.getSeleniumColumns();
                for (SeleniumColumn seleniumColumn : seleniumColumns) {
                    if (seleniumColumn.isUseName()) {
                        if (seleniumColumn.getRowName().contains(name) && seleniumColumn.getFirstColumnText().contains(online)) {
                            multipleColumnSearchResults.add(seleniumColumn);
                        }
                    } else {
                        if (seleniumColumn.getRowValue().contains(name) && seleniumColumn.getFirstColumnText().contains(online)) {
                            multipleColumnSearchResults.add(seleniumColumn);
                        }
                    }
                }
            }
        } catch (NullPointerException npee) {
        }
        return multipleColumnSearchResults;
    }
}
