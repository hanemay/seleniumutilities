package com.csc.testing.seleniumUtils.tables;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class SeleniumTableRow {
    private WebElement fullTable;
    private ArrayList<SeleniumColumn> seleniumColumns;
    private SeleniumColumn columnHeaders = new SeleniumColumn();
    private List<String> columnHeaderNames = new ArrayList<>();

    public ArrayList<SeleniumColumn> getSeleniumColumns() {
        return seleniumColumns;
    }


    public void setSeleniumColumns(ArrayList<SeleniumColumn> seleniumColumns) {
        this.seleniumColumns = seleniumColumns;
    }

    public void setSeleniumRows(List<WebElement> rows, boolean hasColumnHeaders) {
        seleniumColumns = new ArrayList<>();
        int counter = 0;
        String firstColumnText = rows.get(0).getText();
        for (WebElement element : rows) {
            SeleniumColumn seleniumColumn = new SeleniumColumn();
            element = getInputFieldIfExists(element);
            seleniumColumn.setInputField(element);
            seleniumColumn = getRowHeaders(hasColumnHeaders, seleniumColumn, counter);
            seleniumColumn.setFirstColumnInRow(firstColumnText);
            seleniumColumns.add(seleniumColumn);
            if (counter == columnHeaderNames.size() - 1) {
                counter = 0;
            } else {
                counter++;
            }
        }
    }

    private SeleniumColumn getRowHeaders(boolean hasColumnHeaders, SeleniumColumn seleniumColumn, int counter) {
        if (hasColumnHeaders) {
            seleniumColumn.setUseName(true);
            seleniumColumn.setRowName(columnHeaderNames.get(counter));
            return seleniumColumn;
        } else {
            return seleniumColumn;
        }
    }

    private WebElement getInputFieldIfExists(WebElement element) {
        if (hasInput(element)) {
            return element.findElement(By.xpath("./descendant::input"));
        } else {
            return element;
        }
    }

    private String getRowValue(WebElement element) {
        String rowValue = element.getText();
        if (rowValue.equals("")) {
            rowValue = element.getAttribute("value");
        }
        return rowValue;
    }

    private boolean hasInput(WebElement element) {
        try {
            element.findElement(By.xpath("./descendant::input"));
            return true;
        } catch (NoSuchElementException npee) {
            return false;
        }
    }

    public SeleniumColumn getColumnHeaders() {
        return columnHeaders;
    }

    public void setColumnHeaders(WebElement element, List<WebElement> headers) {
        columnHeaders.setHasColumnHeaders(true);
        columnHeaders.setUseName(true);
        for (WebElement header : headers) {
            columnHeaderNames.add(header.getText());
        }
        columnHeaders.setRowHeaders(columnHeaderNames);
    }

    public List<String> getColumnHeaderNames() {
        return columnHeaderNames;
    }

    public WebElement getFullTable() {
        return fullTable;
    }

    public void setFullTable(WebElement fullTable) {
        this.fullTable = fullTable;
    }
}
