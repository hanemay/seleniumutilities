package com.csc.testing.seleniumUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;

import static com.csc.testing.seleniumUtils.findElementStrings.NORMALIZEDOT;
import static java.lang.Thread.sleep;

public class ElementWaiter {

    public boolean waitForElementToAppear(WebDriver driver, LocalDateTime expires, String lookFor) {
        String result;
        while (LocalDateTime.now().isBefore(expires)) {
            try {
                result = driver.findElement(By.xpath(NORMALIZEDOT + lookFor + "']")).getText();
                if (result.equalsIgnoreCase(lookFor)) {
                    return true;
                }
            } catch (NoSuchElementException ex) {
                try {
                    sleep(1000);
                } catch (InterruptedException ignore) {
                }
            }
        }
        return false;
    }
}
