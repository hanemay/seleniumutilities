package com.csc.testing.seleniumUtils;


import com.csc.testing.seleniumUtils.tables.TableFiller;
import com.google.common.base.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.csc.testing.seleniumUtils.HashTagKoder.returnPartialHashTag;
import static com.csc.testing.seleniumUtils.findElementStrings.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;


/**
 * Created by nbade2 on 19-06-2017.
 */
public class SeleniumUtils {

    private static final String ENDNORMALIZE = "']";
    private Integer globalTimeout = 10000;
    private Integer originalTimeout = 0;

    public Integer getGlobalTimeout() {
        return globalTimeout;
    }

    public void setGlobalTimeout(Integer globalTimeout) {
        this.globalTimeout = globalTimeout;
    }

    //sets timeout to originalTimeout
    public void setGlobalTimeoutToOriginal() {
        this.globalTimeout = this.originalTimeout;
    }

    public TableFiller tableFiller;
    public StringManipulation stringManipulation = new StringManipulation();
    //base url the framework is using ie. www.google.com
    @Autowired
    public String url;
    //webdriver
    @Autowired
    private WebDriver driver;
    //javascript calls
    @Autowired
    private JavascriptExecutions javascriptExecutions;
    private boolean doWait = true;
    private SeleniumSetup seleniumSetup;

    public SeleniumUtils(WebDriver driver, JavascriptExecutions javascriptExecutions) {
        this.driver = driver;
        this.javascriptExecutions = javascriptExecutions;
    }

    public SeleniumUtils(SeleniumSetup seleniumSetup) {
        setup(seleniumSetup);
    }
    private void setup(SeleniumSetup seleniumSetup){
        this.seleniumSetup = seleniumSetup;
        if (driver != null && javascriptExecutions != null) {
            seleniumSetup.setAutoWired(true);
        }
        if(seleniumSetup.isCustomTableFiller()){
            this.tableFiller = seleniumSetup.getTableFiller();
        }else{
            this.tableFiller = new TableFiller(this);
        }
        if (seleniumSetup.isLogging()) {

        }
        if (seleniumSetup.isUseGlobalWait()) {
            this.globalTimeout = seleniumSetup.getGlobalTimeOut();
            this.originalTimeout = this.globalTimeout;
        }
        if (seleniumSetup.isOverLoadDriver()) {
            this.driver = seleniumSetup.getDriver();
        }
    }
    public void overRideSettings(SeleniumSetup seleniumSetup){
        setup(seleniumSetup);
    }
    public void overriderTimeOut(Integer globalTimeOut){
        this.globalTimeout = globalTimeOut;
    }
    public void restoreGlobalTimeout(){
        this.globalTimeout = this.seleniumSetup.getGlobalTimeOut();
    }

    public SeleniumUtils() {
        this.tableFiller = new TableFiller(this);
    }

    /**
     * ONLY use this method if hamcrest contains doesn't work
     *
     * @return true or false
     */
    public static org.hamcrest.Matcher<String> contains(String substring) {
        return org.hamcrest.core.StringContains.containsString(substring);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param selectors "se, læse for at læse antal tegn" default er write
     * @param element   element at skrive/læse til/fra
     * @param antalTegn antal tegn der skal assertes eller skrives
     */
    public void sendAmountOfText(String selectors, WebElement element, int antalTegn, Boolean letterAndNumbersOnly) {
        String selector = selectors.toLowerCase();
        StringBuilder result = new StringBuilder();
        switch (selector) {
            case "se":
            case "læse": {
                result = new StringBuilder(element.getAttribute(VALUE));
                assertThat("Antal tegn:", result.length(), is(antalTegn));
                break;
            }
            default:
                sendText(element, result, antalTegn, letterAndNumbersOnly);
                break;
        }
    }

    private void sendText(WebElement element, StringBuilder result, int antalTegn, Boolean letterAndNumbersOnly) {
        element.clear();
        Random random = new Random();
        String alphabet = "0123456789abcdefghijklmnopqrstuvxyzæøå<>'()[]{}?+`´|!\"#¤%&@£$€ ";
        if (letterAndNumbersOnly) {
            alphabet = "0123456789abcdefghijklmnopqrstuvxyzæøå";
        }
        alphabet += alphabet.toUpperCase();
        for (int i = 0; i < antalTegn; i++) {
            result.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }
        element.sendKeys(result.toString());
        element.sendKeys(Keys.TAB);
    }


    /**
     * @param indholdBrevskabelon the list
     * @return true if successfull
     */
    public boolean waitForElementByPageSource(List<String> indholdBrevskabelon) {
        int checker = 0;
        boolean getNext;
        String lastElement;
        for (String stringToValidate : indholdBrevskabelon) {
            getNext = false;
            lastElement = stringToValidate;
            while (!getNext) {
                if (checker >= 10000) {
                    throw new AssertionError(TIMEOUT + checker + COULDNOTFINDELEMENT + lastElement);
                }
                try {
                    assertTrue(driver.getPageSource().contains(stringToValidate));
                    getNext = true;
                    sleep(100);
                    checker += 100;
                } catch (AssertionError ae) {
                    sleep(100);
                    checker += 100;
                }
            }
        }
        return true;
    }


    public String getCheckBoxValue(String linktekst) {
        WebElement checkbox = findForValue(linktekst);
        return checkbox.getAttribute("checked");
    }

    /**
     * get value for a specific field ie checkbox textarea and text field very time consuming
     *
     * @param value value to find ie: "Log ud"
     * @return the text for that field
     */
    public String getTextByXpath(String value) {
        String returnValue;
        boolean bGetText = false;
        String getAttribute = getAttributeOnElement(value);
        switch (getAttribute) {
            case "checkbox":
            case "radio":
                getAttribute = ATTRIBUTECHECKED;
                break;
            default:
                getAttribute = VALUE;
                bGetText = true;
                break;
        }
        try {
            if (bGetText) {
                returnValue = findForValue(value).getText();
            } else {
                throw new NoSuchElementException("");
            }
            if ("".equals(returnValue)) {
                throw new NoSuchElementException(EMPTY);
            }
        } catch (NoSuchElementException e) {
            try {
                returnValue = driver
                        .findElement(By.xpath(IDANDNORMALIZESPACEDOT + value + ENDFOR))
                        .getAttribute(getAttribute);
            } catch (NoSuchElementException e0) {
                returnValue = driver.findElement(By.xpath(IDANDNORMALIZESPACETEXT + value + ENDFOR))
                        .getAttribute(getAttribute);
            }
            if (returnValue == null && getAttribute.equals(ATTRIBUTECHECKED)) {
                returnValue = FALSE;
            }
        }
        return returnValue;
    }

    public WebElement findForValue(String value) {
        String idToFind = null;
        try {
            idToFind = findElementWithNormalizeSpace(value).getAttribute("for");
        } catch (NoSuchElementException ignored) {
        }
        try {
            if (idToFind == null) {
                idToFind = findElementWithNormalizeSpaceText(value).getAttribute("for");
            }
        } catch (NoSuchElementException ignored) {
        }
        try {
            if (idToFind == null) {
                idToFind = findElementByXpathAndWait(NORMALIZEDOT + value + "']/*").getAttribute("for");
            }
        } catch (NoSuchElementException ignored) {
        }

        if (idToFind == null) {
            throw new NoSuchElementException(
                    "Kunne ikke finde et element med en for-value hvor teksten er '" + value + "'");
        }

        return findElementByIdAndWait(idToFind);
    }

    public List<WebElement> findListByForValue(String value) {
        String idToFind = null;
        try {
            idToFind = findElementWithNormalizeSpaceText(value).getAttribute("for");
        } catch (NoSuchElementException ignored) {

        }
        try {
            if (idToFind == null) {
                idToFind = findElementWithNormalizeSpace(value).getAttribute("for");
            }
        } catch (NoSuchElementException ignored) {

        }
        if (idToFind == null) {
            idToFind = findElementByXpathAndWait(NORMALIZEDOT + value + "']/*").getAttribute("for");
        }
        return findElementsByIdAndWait(idToFind);
    }

    private String getAttributeOnElement(String value) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        String getAttribute = "";
        WebElement element = findForValue(value);
        try {
            getAttribute = element.getAttribute("type");
        } catch (NoSuchElementException textException) {

            try {
                getAttribute = element.getAttribute("role");
            } catch (NoSuchElementException nsee) {
                try {
                    getAttribute = element.getAttribute("type");
                } catch (NoSuchElementException ignored) {

                }
            }
        }
        return getAttribute;
    }

    /**
     * get value for a specific field ie checkbox textarea and text field
     *
     * @param value value to find ie: "Log ud"
     * @return the text for that field
     */
    public String getTextById(String value) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        String returnValue;
        boolean bGetText = false;
        String getAttribute = driver.findElement(By.id(value)).getAttribute("role");
        if (getAttribute.equals("checkbox")) {
            getAttribute = ATTRIBUTECHECKED;
        } else {
            getAttribute = VALUE;
            bGetText = true;
        }
        try {
            if (bGetText) {
                returnValue = findElementByByAndWait(null, By.id(value)).getText();
            } else {
                throw new NoSuchElementException("");
            }
            if (returnValue.equals("")) {
                throw new NoSuchElementException(EMPTY);
            }
        } catch (NoSuchElementException e) {
            returnValue = findElementByByAndWait(null, By.id(value)).getAttribute(getAttribute);
        }
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        return returnValue;
    }


    /**
     * run this method to check if a field is disabled, consider using viseValideringsfejl(String
     * feltLabel, String selector) if ng-invalid og ng-valid is what youre looking for
     *
     * @param field the field to test against
     */
    public void getLockedElement(String field, boolean isLocked) {

        String assertThisString;
        if (isLocked) {
            assertThisString = "true";
        } else {
            assertThisString = FALSE;
        }
        try {
            findElementByXpathAndWait(NAMEWITHFOR + field + FIELDSETDISABLED);
        } catch (NoSuchElementException nsee) {

            try {
                assertThat("Should be " + assertThisString,
                        findElementByXpathAndWait(IDANDNORMALIZESPACETEXT + field + "']/@for)]")
                                .getAttribute(DISABLED), is(assertThisString));
            } catch (NoSuchElementException nsee1) {
                try {
                    findElementByXpathAndWait(NAMEWITHFOR + field + FIELDSETDISABLED);
                } catch (Exception e) {

                    try {
                        findElementByXpathAndWait(
                                "//*[@id='" + field + "']/following::*/@disabled='disabled'");
                    } catch (Exception ee) {
                        try {
                            findElementByXpathAndWait("//*[@id='" + field + "']/fieldset[@disabled]");
                        } catch (Exception secondLast) {
                            try {
                                findElementByXpathAndWait(
                                        NAMEWITHFOR + field + "']/following::*/fieldset[@disabled]");
                            } catch (Exception throwFalse) {
                                if (!assertThisString.equals(FALSE)) {
                                    throw new AssertionError(field + " is editable");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * run this method to check if a field is disabled, consider using viseValideringsfejl(String
     * feltLabel, String selector) if ng-invalid og ng-valid is what youre looking for
     *
     * @param field the field to test against
     */
    public String getLockedElement(String field) {

        String result;
        try {
            driver.findElement(By.xpath(NAMEWITHFOR + field + FIELDSETDISABLED));
            result = "true";
        } catch (NoSuchElementException nsee) {

            try {
                result = findElementByXpathAndWait(IDANDNORMALIZESPACETEXT + field + "']/@for)]")
                        .getAttribute(DISABLED);
            } catch (NoSuchElementException nsee1) {

                try {
                    result = findElementByXpathAndWait(NAMEWITHFOR + field + "']/fieldset")
                            .getAttribute(DISABLED);
                } catch (NoSuchElementException nsee2) {

                    result = findElementByXpathAndWait(NAMEWITHFOR + field + "']/*/@disabled='disabled'")
                            .getAttribute(DISABLED);
                }
            }
        }
        return result;
    }

    /**
     * run this method to check if a field is disabled, consider using viseValideringsfejl(String
     * feltLabel, String selector) if ng-invalid og ng-valid is what youre looking for
     *
     * @param field the field to test against
     */
    public String getLockedElementWithAssert(String field, boolean isReadOnly) {

        String result;
        result = NAMEWITHFOR + field + "' and contains(@data-ng-if,'vm.result.readonly')]";
        try {
            result = findElementByXpathAndWait(result).getText();
        } catch (NoSuchElementException nsee) {

            result = null;
        }
        return result;
    }

    /**
     * use this method to send text if element is not an input it will send an new line
     *
     * @param msg  the text to parse
     * @param elem the element to parse at
     */
    public void sendText(String msg, WebElement elem) {
        if (!"input".equals(elem.getTagName())) {
            elem.sendKeys(Keys.chord(Keys.SHIFT, Keys.ENTER), msg);
        } else {
            elem.sendKeys(msg);
            elem.sendKeys(Keys.TAB);
        }

    }


    /**
     * Valadate Toaster
     *
     * @param message String which the toaster should represent
     */
    public void validateToaster(String message) {
        boolean skipThrow = false;
        if ("null".equals(message)) {
            skipThrow = true;
        }
        for (int i = 0; i < 25; i++) {
            try {
                List<WebElement> webElements = findElementsByIdAndWait("toast-container");
                for (WebElement element : webElements) {
                    if (element.getText().contains(message)) {
                        return;
                    }
                }
            } catch (NoSuchElementException nsee) {
                if (!skipThrow) {
                    throw nsee;
                }
            }
        }
        if (!skipThrow) {
            throw new AssertionError("kunne ikke finde en toaster indeholdende '" + message + "'");
        }
    }

    /**
     * Valadate Modal
     *
     * @param message String which the toaster should represent
     */
    public void validateModal(String message) {
        String asterixXpathWithTripple =
                "//*[@class='modal-content']/*/*/*[text()=\"" + message + "\"]";
        String asterixXpathWithDouble = "//*[@class='modal-content']/*/*[text()=\"" + message + "\"]";
        String xpathOnlyText = NORMALIZETEXT + message + "']";
        try {
            findElementByXpathAndWait(asterixXpathWithTripple);
        } catch (NoSuchElementException nsee) {
            try {
                findElementByXpathAndWait(asterixXpathWithDouble);
            } catch (NoSuchElementException nsee1) {
                try {
                    findElementByXpathAndWait(xpathOnlyText);
                } catch (NoSuchElementException last) {
                    throw new NoSuchElementException("Tried \n " + asterixXpathWithTripple +
                            "and " + asterixXpathWithDouble + " and \n" + xpathOnlyText, nsee);
                }
            }
        }
    }


    /**
     * @param linkTekst press on a button where text == linkTekst
     */
    public void trykPaa(String linkTekst) {
        for (int trykPaaCounter = 0; trykPaaCounter < 3; trykPaaCounter++) {
            try {
                if (linkTekst.contains("#" + "")) {
                    clickHashTag(linkTekst);
                } else {
                    trykPaaFoerste(linkTekst);
                }
                break;
            } catch (Exception stop) {
                if (trykPaaCounter >= 2) {
                    throw new NoSuchElementException(
                            "kunne ikke finde elementet, stopper for tekst: " + linkTekst);
                }
            }
        }
    }

    private boolean clickIfButton(List<WebElement> elements, String linkTekst) {
        scrollTo(elements.get(0));
        if (!findButtonAndClick(elements, linkTekst)) {
            clickWithElement(elements.get(0));
            return true;
        }

        return false;
    }

    private void clickWithElement(WebElement element) {
        scrollTo(element);
        if (!element.isEnabled()) {
            throw new AssertionError(
                    "kunne ikke trykke på " + element.getText() + " da " + element.getText()
                            + " var disabled");
        }
        javascriptExecutions.clickWithJavascript(element);
    }

    private void trykPaaFoerste(String linkTekst) {
        try {
            clickWithElement(driver.findElement(By.linkText(linkTekst)));
        } catch (Exception nsee) {
            try {
                clickIfButton(findElementsByXpathAndWait(NORMALIZETEXT + linkTekst + "']"), linkTekst);
            } catch (Exception e) {
                try {
                    clickIfButton(findElementsByXpathAndWait(NORMALIZEDOT + linkTekst + "']"),
                            linkTekst);
                } catch (Exception f1) {
                    try {
                        javascriptExecutions.executeJavascript("arguments[0].click();",
                                findElementByXpathAndWait(NORMALIZETEXT + linkTekst + "']"));
                    } catch (Exception f2) {
                        try {
                            findElementByXpathAndWait("//*[contains(text(),'" + linkTekst + "')]").click();
                        } catch (Exception f3) {
                            try {
                                WebElement element = findElementByXpathAndWait(
                                        NORMALIZEDOT + linkTekst + "']");
                                clickScreenReaderButton(element);
                            } catch (Exception f4) {
                                throw new NoSuchElementException("kunne ikke finde " + linkTekst);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Prøver at trykke på en screenreader knap. Det vil være en knap med et button tag med et span
     * tag indeni som man ikke kan klikke på
     *
     * @param element Paretn elementet som kan indeholde knappen
     */
    private boolean clickScreenReaderButton(WebElement element) {
        if (element == null) {
            return false;
        }

        // Hvis det er en div med en button, så find child elementet som er en knap og tryk på den
        if ("div".equalsIgnoreCase(element.getTagName())) {
            try {
                WebElement screenReaderButton = ((RemoteWebElement) element).findElementByTagName("button");
                if (screenReaderButton != null) {
                    screenReaderButton.click();
                    return true;
                }
            } catch (NoSuchElementException e) {
                WebElement screenReaderButton = ((RemoteWebElement) element).findElementByTagName("a");
                if (screenReaderButton != null) {
                    screenReaderButton.click();
                    return true;
                }
            }
        }
        return false;
    }

    private void clickHashTag(String linkTekst) {
        char upper = linkTekst.charAt(0);
        String clickMe = returnPartialHashTag(linkTekst.toLowerCase());
        if (Character.isUpperCase(upper)) {
            clickMe = upper + clickMe.substring(1, clickMe.length());
        }
        try {
            trykPaa(clickMe);
        } catch (NoSuchElementException e) {
            clickElement(clickMe);
        }
    }

    private boolean findButtonAndClick(List<WebElement> elements, String linkTekst) {
        for (WebElement element : elements) {
            if (element.getTagName().equals("button")) {
                if (!element.isEnabled()) {
                    throw new AssertionError(
                            "kunne ikke trykke på " + linkTekst + " da " + linkTekst + " var disabled");
                }
                element.click();
                sleep(200);
                return true;
            }
        }
        return false;
    }

    /**
     * Validate Toaster
     */
    public void validateToasterWithField(String field) {
        findElementByXpathAndWait("//*[contains(text(),'" + field + "')]");
    }

    /**
     * Valadate Toaster
     *
     * @param felt   field to point at ie "bidragstyper", "ejendomsoversigt"
     * @param status false for invalid, true for valid
     */
    public void validateToaster(String felt, String status) {
        if (status.contains("ikke")) {
            findElementByXpathAndWait(
                    IDANDNORMALIZESPACETEXT + felt + "']/@for) and not(contains(@class, 'ng-invalid'))]");
        } else {
            findElementByXpathAndWait(
                    IDANDNORMALIZESPACETEXT + felt + "']/@for) and contains(@class, 'ng-invalid')]");
        }
    }


    public WebElement findElementByXpathAndWaitWithContains(String elementToFind,
                                                            String contains) {
        int checker = 0;
        WebElement element = null;
        while (checker < 10000) {
            try {
                element = driver.findElement(By.xpath(elementToFind));
                if (element != null && element.getText().toLowerCase().contains(contains)) {
                    break;
                }
            } catch (Exception ignored) {

            }
            sleep(100);
            checker += 100;
        }
        if (checker < 10000) {
            return element;
        } else {
            throw new NoSuchElementException(TIMEOUT + checker + COULDNOTFINDELEMENT + elementToFind);
        }
    }


    /**
     * finds a table and returns it as a list with a table name and text what column to find ie ("Ejer
     * information","Navn")
     *
     * @param tableToFind   the text above the table
     * @param elementToFind the column in the table to find
     * @return a list with all rows for that column
     */
    public List<WebElement> findTableByPartialXpath(String tableToFind, String elementToFind) {
        int checker = 0;
        List<WebElement> elements = null;
        while (checker < 10000) {
            try {
                elements = driver.findElements(By.xpath(
                        NORMALIZETEXT + tableToFind + "']/following::*/*[@aria-labelledby=(" + NORMALIZETEXT
                                + elementToFind + "'])/@id]"));
                if (!elements.isEmpty() && elements.size() != 0) {
                    break;
                }
            } catch (Exception ignored) {

            }
            sleep(100);
            checker += 100;
        }
        if (checker < 10000) {
            return elements;
        } else {
            throw new NoSuchElementException(TIMEOUT + checker + COULDNOTFINDELEMENT + elementToFind);
        }
    }

    /**
     * finds a table and returns it as a list with a table name and text what column to find ie ("Ejer
     * information","Navn")
     *
     * @return a list with all rows for that column
     */
    public List<WebElement> findListByPartialXpath() {
        int checker = 0;
        List<WebElement> elements = null;
        while (checker < 10000) {
            try {
                elements = driver.findElements(By.xpath("//*[@class='filtered_list']/*"));
                if (!elements.isEmpty()) {
                    break;
                }
            } catch (Exception ignored) {

            }
            sleep(100);
            checker += 100;
        }
        if (checker < 10000) {
            return elements;
        }

        return elements;
    }

    /**
     * @param millis tiden skal sleep i
     */
    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {

        }
    }

    /**
     * click element by tekst
     *
     * @param text clicks element based on this string
     */
    private void clickElement(String text) {
        try {
            try {
                findElementByXpathAndWait(text).click();
            } catch (Exception e) {

                try {
                    findElementByIdAndWait(text).click();
                } catch (Exception ex) {

                    try {
                        findElementByClassNameAndWait(text).click();
                    } catch (Exception ec) {

                        try {
                            findElementByNgClickAndWait(text).click();
                        } catch (Exception ecLast) {
                            throw new NoSuchElementException("kunne ikke finde elementet: " + text);
                        }
                    }
                }
            }
        } catch (Exception ignored) {

        }
    }


    /**
     * return date for #nuDag.#nuMåned.#nuværendeÅr or 01.#nuMåned.#nuværendeÅr etc.
     */
    public String hashTagDate(String dato) {
        if (dato.contains("#") && dato.contains(".")) {
            String[] dates = dato.split("\\.");
            for (int i = 0; i < dates.length; i++) {
                if (dates[i].contains("#")) {
                    dates[i] = returnPartialHashTag(dates[i]);
                }
            }
            return dates[0] + "." + dates[1] + "." + dates[2];
        }
        return dato;
    }

    /**
     * return element number for a element from a List<WebElement>
     */
    public int getElementNumber(List<WebElement> elements, String kolonne) {
        int pos = 0;
        for (WebElement element : elements) {
            pos++;
            if (element.getText().equals(kolonne)) {
                return pos;
            }
        }
        return -1;
    }

    /**
     * sets first letter of a given string to upper case
     *
     * @param getElement the minimum of elements you will have
     */
    public List<WebElement> getRowFromList(String xpath, int getElement) {
        boolean isToSmall = false;
        int count = 0;
        int isZero = 0;
        List<WebElement> webElements = null;
        tryAgain:
        while (!isToSmall) {
            webElements = findElementsByXpathAndWait(xpath);
            if (webElements.size() > getElement - 1) {
                if (webElements.size() == isZero && isZero == 0) {
                    isZero++;
                    break tryAgain;
                }
                break;
            } else {
                sleep(250);
                count++;
            }
            if (count == 10) {
                isToSmall = true;
            }
        }
        return webElements;
    }


    public String checkForHashTags(String value) {
        if (value != null && value.contains("#")) {
            value = returnPartialHashTag(value);
        }
        return value;
    }

    public List<WebElement> findElementsByXpathAndWait(String elementsToFind) {
        return findElementsByByAndWait(null, By.xpath(elementsToFind));
    }

    public List<WebElement> findElementsByXpathAndWait(WebElement element,
                                                       String elementsToFind) {
        return findElementsByByAndWait(element, By.xpath(elementsToFind));
    }

    private List<WebElement> findElementsByIdAndWait(String elementsToFind) {
        return findElementsByByAndWait(null, By.id(elementsToFind));
    }

    public List<WebElement> findElementsByIdAndWait(WebElement element,
                                                    String elementsToFind) {
        return findElementsByByAndWait(element, By.id(elementsToFind));
    }

    public List<WebElement> findElementsByClassNameAndWait(String elementsToFind) {
        return findElementsByByAndWait(null, By.className(elementsToFind));
    }

    public List<WebElement> findElementsByClassNameAndWait(WebElement element,
                                                           String elementsToFind) {
        return findElementsByByAndWait(element, By.className(elementsToFind));
    }

    /**
     * @param element  element der skal appendes til
     * @param by       xpath class id
     * @param timeInMS max time for wait
     * @return element fundet inde i Element
     */
    private List<WebElement> findElementsByByAndWait(WebElement element, By by) {
        try {
            boolean found = false;
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofMillis(globalTimeout))
                    .pollingEvery(Duration.ofMillis(globalTimeout / 100))
                    .ignoring(NoSuchElementException.class);

            boolean useNestedElement = useNestedElement(element);
            List<WebElement> elements = wait.until(new Function<WebDriver, List<WebElement>>() {
                public List<WebElement> apply(WebDriver driver) {
                    if (useNestedElement) {
                        return element.findElements(by);

                    } else {
                        return driver.findElements(by);
                    }
                }
            });
            if (elements.size() == 0) {
                throw new NoSuchElementException("kunne ikke finde " + by);
            }
            return elements;
        } catch (TimeoutException tme) {
            throw new NoSuchElementException("kunne ikke finde " + by);
        }
    }

    public WebElement findElementByClassNameAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.className(elementToFind));
    }

    /**
     * find element with partial xpath ie: "Log ud"
     *
     * @param elementToFind element to look for
     * @return a web element to work with
     */
    public WebElement findElementByPartialXpathAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.xpath(IDANDNORMALIZESPACEDOT + elementToFind + ENDFOR)
        );
    }

    private WebElement findElementByNgClickAndWait(String text) {
        return findElementByByAndWait(null, By.xpath("//*[@ng-click=(\"" + text + "\")]"));
    }

    /**
     * find element with partial xpath ie: "Log ud"
     *
     * @param elementToFind element to look for
     * @return a web element to work with
     */
    private WebElement findElementByClassAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.xpath(IDANDNORMALIZESPACETEXT + elementToFind + ENDFOR)
        );
    }

    public WebElement findElementByIdAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.id(elementToFind));
    }

    /**
     * Leder efter aria-labelledby eller aria-label tag
     *
     * @param elementToFind aria navnet der skal ledes efter
     * @return elementet eller en exception
     */
    public WebElement findElementByAriaLabelAndWait(String elementToFind, boolean ignoreOtherButtons) {
        List<WebElement> elements;
        try {
            elements = findElementsByXpathAndWait("//*[@aria-labelledby='" + elementToFind + "']");
        } catch (NoSuchElementException e) {
            elements = findElementsByXpathAndWait("//*[@aria-label='" + elementToFind + "']");
        }

        if (!ignoreOtherButtons && elements.size() != 1) {
            throw new NotFoundException("Der blev fundet " + elements.size() + ". Der burde kun være 1");
        }

        return elements.get(0);
    }

    public WebElement findElementByXpathAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.xpath(elementToFind));
    }

    public WebElement findElementWithNormalizeSpace(String elementToFind) {
        return findElementByByAndWait(null, By.xpath(NORMALIZEDOT + elementToFind + ENDNORMALIZE));
    }

    public WebElement findElementWithNormalizeSpaceText(String elementToFind) {
        return findElementByByAndWait(null, By.xpath(NORMALIZEDOT + elementToFind + ENDNORMALIZE));
    }

    public String findeText(String text) {
        return findElementByXpathAndWait(NORMALIZEDOT + text + "']").getText();
    }

    public WebElement findElementByXpathAndWait(WebElement element,
                                                String elementToFind) {
        return findElementByByAndWait(element, By.xpath(elementToFind));
    }

    public WebElement findElementByLinktextAndWait(String elementToFind) {
        return findElementByByAndWait(null, By.linkText(elementToFind));
    }

    public WebElement findElementByLinktextAndWait(WebElement element,
                                                   String elementToFind) {
        return findElementByByAndWait(element, By.linkText(elementToFind));
    }

    public List<WebElement> findElementsByLinktextAndWait(String elementToFind) {
        return findElementsByByAndWait(null, By.linkText(elementToFind));
    }

    public List<WebElement> findElementsByLinktextAndWait(WebElement element,
                                                          String elementToFind) {
        return findElementsByByAndWait(element, By.linkText(elementToFind));
    }


    /**
     * find element with id alone
     *
     * @param timeInMS time to wait >100
     * @return a web element to work with
     */
    private WebElement findElementByByAndWait(WebElement element, By by) {
        try {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofMillis(globalTimeout))
                    .pollingEvery(Duration.ofMillis(globalTimeout / 100))
                    .ignoring(NoSuchElementException.class);
            boolean useNestedElement = useNestedElement(element);
            return wait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    if (useNestedElement) {
                        return element.findElement(by);
                    } else {
                        return driver.findElement(by);
                    }
                }
            });
        } catch (TimeoutException tme) {
            throw new NoSuchElementException("kunne ikke finde " + by);
        }
    }

    private boolean useNestedElement(WebElement element) {
        return element != null;
    }

    /**
     * simulere f5
     */
    public void updateCurrentView() {

        sleep(1000);
        driver.navigate().refresh();
        findElementByXpathAndWait("//*");

        sleep(2000);
    }

    /**
     * venter på siden er loaded
     */
    public void waitForLoadedPage() {
        if (this.doWait)
            checkPageIsReady();
    }

    /**
     * samme som waitForLoadedPag, her bliver der dog brugt et while loop som henter sources
     */
    public void extremeWait() {
        int counter = 0;
        boolean sourceCheck = false;
        checkPageIsReady();
        while (!sourceCheck) {
            ArrayList<String> sources = getSource(40);
            sourceCheck = true;
            for (String source : sources) {
                if (!source.equals(sources.get(0))) {
                    sourceCheck = false;
                }
            }
            if (counter == 10) {
                throw new AssertionError("kunne ikke indlæse siden færdigt");
            } else {
                counter++;
            }
        }
    }

    /**
     * checks if ready state == complete
     */
    public void checkPageIsReady() {
        String angularWait = "return (window.angular !== undefined) && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)";
        //initial state
        if (javascriptExecutions.executeJavascript(angularWait).equals("true")) {
            return;
        }
        for (int i = 0; i < 25; i++) {
            sleep(100);
            //To check page ready state.
            if (javascriptExecutions.executeJavascript(angularWait).equals("true")) {
                break;
            }
        }
    }

    /*
    get page source, brugs til this.extremeWait()
     */
    private ArrayList<String> getSource(int i) {
        ArrayList<String> sources = new ArrayList<>();
        sources.add(driver.getPageSource());
        sleep(100);
        for (int counter = 0; counter < i - 1; counter++) {
            sleep(50);
            sources.add(driver.getPageSource());
        }
        return sources;
    }

    /**
     * @return toaster eror trur/false
     */
    public boolean checkForErrorModalAndRestart() {
        try {
            return findElementByIdAndWait("toast-container").getAttribute("class")
                    .contains("toast-error");
        } catch (NoSuchElementException nsee) {
            return false;
        }
    }

    /**
     * @param element       element der skal testes på
     * @param selector      selector aktiv/inaktiv
     * @param elementToFind navnet på elementet man har fundet, dvs den selector der er brugt til at
     *                      finde element med
     */
    public void doesElementExist(WebElement element, String selector, String elementToFind) {
        switch (selector) {
            case "aktiv":
                try {
                    if (element.getAttribute(DISABLED) != null) {
                        throw new AssertionError("Knappen " + elementToFind + " er ikke " + selector);
                    }
                } catch (Exception ignored) {

                }
                break;
            case "inaktiv":
                try {
                    // Check if we have a span - if we do it's a button with a span in it
                    String disabledAttributte = element.getAttribute(DISABLED);
                    if (disabledAttributte == null) {
                        // Get the parent (the button)
                        element = element.findElement(By.xpath("parent::*"));
                    }
                    assertThat(element.isEnabled(), is(false));
                } catch (Exception aiiobe) {
                    throw new AssertionError("Knappen " + elementToFind + " er ikke " + selector);
                }
                break;
            default:
                throw new AssertionError("error ingen selector sat");
        }
    }

    /**
     * @param element element der skal scrolles til
     */
    public void scrollTo(WebElement element) {
        if (element != null) {
            javascriptExecutions.executeJavascript(
                    SCROLLTO + "(" + String.valueOf(element.getLocation().getX() - 100) + "," + String.valueOf(element.getLocation().getY() - 200) + ")" + ";");
        }
    }

    /**
     * denne metode får musen til at hover over et menupunkts dropdown
     *
     * @param menupunkt menu punkt som musen skal køre over
     */
    public void activateMenu(String menupunkt) {
        WebElement div_menu = findElementByXpathAndWait(NORMALIZETEXT + menupunkt + "']");
        Actions builder = new Actions(driver);
        builder.moveToElement(div_menu).build().perform();
        waitForLoadedPage();
    }

    public void trykPaaIkkeDisabled(String linkTekst) {
        clickIfButtonNotDisabled(findElementsByXpathAndWait(NORMALIZETEXT + linkTekst + "']"), linkTekst);
    }

    private void clickIfButtonNotDisabled(List<WebElement> elements, String linkTekst) {
        boolean throwException = true;
        for (WebElement element : elements) {
            if (element.isEnabled() && element.isDisplayed()) {
                element.click();
                throwException = false;
                break;
            }
        }
        if (throwException) {
            throw new AssertionError("kunne ikke trykke på " + linkTekst);
        }
    }

    public void writeInField(String value, String fieldName) {
        WebElement field = findForValue(fieldName);
        field.sendKeys(value);
        field.sendKeys(Keys.TAB);
    }

    public void findFieldAndSendText(String navn, String performanceTest) {
        WebElement field = findForValue(navn);
        field.clear();
        field.sendKeys(performanceTest);
        this.sleep(100);
    }

    public void scrollToAndClick(WebElement element) {
        scrollTo(element);
        element.click();
    }

    public void doWait(boolean doWait) {
        this.doWait = doWait;
    }

    public void pasteKeys(String keys, WebElement element) {
        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection testData;
        testData = new StringSelection(keys);
        c.setContents(testData, testData);
        Actions act = new Actions(driver);
        act.moveToElement(element).doubleClick().build().perform();
        element.sendKeys(Keys.chord(Keys.CONTROL, "v"));
    }


    public void waitForTextToAppear(String text, int times) {
        boolean throwException = true;
        for (int i = 0; i < times; i++) {
            try {
                findeText(text);
                throwException = false;
                i = times;
            } catch (NoSuchElementException nse) {

            }
        }
        if (throwException) {
            throw new NoSuchElementException("kunne ikke finde " + text + " efter " + times + " forsøg");
        }
    }

    public void closeTabs() {
        ArrayList<String> tabList = new ArrayList<>(driver.getWindowHandles());
        for (int i = 1; i < tabList.size(); i++) {
            driver.switchTo().window(tabList.get(i)).close();
        }
        driver.switchTo().window(tabList.get(0));
    }


}
